<?php

function _service_introspect_addFunc($module, $action, $http_method, $method){
  $out = array(
    'method' => $http_method,
    'url' => $module . '/' . $action,
    'args' => array(
      'path' => array(),
      'data' => array(),
      'param' => array()
    )
  );
  
  foreach($method['args'] as $arg){
    $argType='param';
    if ($arg['source'] == 'data'){
      $argType='data';
    }elseif(isset($arg['source']['path'])){
      $argType='path';
    }
    $out['args'][$argType][$arg['name']] = $arg['optional'];
  }

  return $out;
}

function services_introspect_index() {
  $info=module_invoke_all('services_resources');
  $out=array();

  $method_map = array(
    'retrieve'         => 'GET',
    'update'           => 'PUT',
    'delete'           => 'DELETE',
    'index'            => 'GET',
    'create'           => 'POST',
    'actions'          => 'POST',
    'targeted actions' => 'POST',
    'relationships'    => 'GET',
  );
  
  foreach($info as $module=>$modules){
    foreach($modules as $action=>$method){      
      $http_method = $method_map[$action];
      if ($action != 'actions' && $action != 'relationships'){
        $out[$module . '.' . $action]=_service_introspect_addFunc($module, $action, $http_method, $method);
      }else if ($action=='actions' || $action=='targeted actions'){
        foreach($method as $a=>$inner_method){
          $out[$module . '.' . $a]=_service_introspect_addFunc($module, $a, $http_method, $inner_method);
        }
      }
    }
  }
  return $out;
}